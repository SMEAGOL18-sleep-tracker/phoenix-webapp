defmodule Sleepweb.Repo.Migrations.CreateTrackers do
  use Ecto.Migration

  def change do
    create table(:trackers) do
      add :name, :string
      add :misc, :map

      timestamps()
    end

  end
end
