defmodule Sleepweb.SMS do
  @moduledoc """
  The SMS context.
  """

  import Ecto.Query, warn: false
  require Logger
  alias Sleepweb.Repo

  alias Sleepweb.SMS.Tracker

  @doc """
  Returns the list of trackers.

  ## Examples

      iex> list_trackers()
      [%Tracker{}, ...]

  """
  def list_trackers do
    Repo.all(Tracker)
  end

  @doc """
  Gets a single tracker.

  Raises `Ecto.NoResultsError` if the Tracker does not exist.

  ## Examples

      iex> get_tracker!(123)
      %Tracker{}

      iex> get_tracker!(456)
      ** (Ecto.NoResultsError)

  """
  def get_tracker!(id), do: Repo.get!(Tracker, id)

  @doc """
  Creates a tracker.

  ## Examples

      iex> create_tracker(%{field: value})
      {:ok, %Tracker{}}

      iex> create_tracker(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tracker(attrs \\ %{}) do
    %Tracker{}
    |> Tracker.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a tracker.

  ## Examples

      iex> update_tracker(tracker, %{field: new_value})
      {:ok, %Tracker{}}

      iex> update_tracker(tracker, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_tracker(%Tracker{} = tracker, attrs) do
    tracker
    |> Tracker.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Tracker.

  ## Examples

      iex> delete_tracker(tracker)
      {:ok, %Tracker{}}

      iex> delete_tracker(tracker)
      {:error, %Ecto.Changeset{}}

  """
  def delete_tracker(%Tracker{} = tracker) do
    Repo.delete(tracker)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking tracker changes.

  ## Examples

      iex> change_tracker(tracker)
      %Ecto.Changeset{source: %Tracker{}}

  """
  def change_tracker(%Tracker{} = tracker) do
    Tracker.changeset(tracker, %{})
  end

  alias Sleepweb.SMS.Data

  @doc """
  Returns the list of data.

  ## Examples

      iex> list_data()
      [%Data{}, ...]

  """
  def list_data do
    Repo.all(Data)
  end

  @doc """
  Gets a single data.

  Raises `Ecto.NoResultsError` if the Data does not exist.

  ## Examples

      iex> get_data!(123)
      %Data{}

      iex> get_data!(456)
      ** (Ecto.NoResultsError)

  """
  def get_data!(id), do: Repo.get!(Data, id)

  @doc """
  Creates a data.

  ## Examples

      iex> create_data(%{field: value})
      {:ok, %Data{}}

      iex> create_data(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_data(attrs \\ %{}) do
    %Data{}
    |> Data.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a data.

  ## Examples

      iex> update_data(data, %{field: new_value})
      {:ok, %Data{}}

      iex> update_data(data, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_data(%Data{} = data, attrs) do
    data
    |> Data.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Data.

  ## Examples

      iex> delete_data(data)
      {:ok, %Data{}}

      iex> delete_data(data)
      {:error, %Ecto.Changeset{}}

  """
  def delete_data(%Data{} = data) do
    Repo.delete(data)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking data changes.

  ## Examples

      iex> change_data(data)
      %Ecto.Changeset{source: %Data{}}

  """
  def change_data(%Data{} = data) do
    Data.changeset(data, %{})
  end



  def latest_bpm_data(sensor) do
    Repo.all(
      from d in Data,
      where: not is_nil(d.bpm),
      where: d.sensor == ^sensor,
      order_by: [desc: d.id],

      select: %{sensor: d.sensor,time: d.time, bpm: d.bpm},
      #select: [:sensor, :time, :bpm],
      limit: 500
    )
  end

  def latest_lin_data(sensor) do
    Repo.all(
      from d in Data,
      where: not is_nil(d.lin_acc),
      where: d.sensor == ^sensor,
      order_by: [desc: d.id],

      select: %{sensor: d.sensor,time: d.time, lin_acc: d.lin_acc},
      limit: 500
    )
  end

  def latest_ang_data(sensor) do
    Repo.all(
      from d in Data,
      where: not is_nil(d.angular_change),
      where: d.sensor == ^sensor,
      order_by: [desc: d.id],

      select: %{sensor: d.sensor,time: d.time, angular_change: d.angular_change},
      limit: 500
    )
  end




  def get_bpm_data(sensor, %NaiveDateTime{} = start_date, %NaiveDateTime{} = end_date) do
    Repo.all(
      from d in Data,
      where: not is_nil(d.bpm),
      where: d.sensor == ^sensor,
      where: d.time >= ^start_date,
      where: d.time <= ^end_date,
      where: d.bpm <= ^120,
      order_by: [desc: d.id],

      select: %{sensor: d.sensor,time: d.time, bpm: d.bpm},
      #select: [:sensor, :time, :bpm],
      limit: 3600
    )
  end

  def get_lin_data(sensor, %NaiveDateTime{} = start_date, %NaiveDateTime{} = end_date) do
    Repo.all(
      from d in Data,
      where: not is_nil(d.lin_acc),
      where: d.sensor == ^sensor,
      where: d.time >= ^start_date,
      where: d.time <= ^end_date,
      order_by: [desc: d.id],

      select: %{sensor: d.sensor,time: d.time, lin_acc: d.lin_acc},
      limit: 3600
    )
  end

  def get_ang_data(sensor, %NaiveDateTime{} = start_date, %NaiveDateTime{} = end_date) do
    Repo.all(
      from d in Data,
      where: not is_nil(d.angular_change),
      where: d.sensor == ^sensor,
      where: d.time >= ^start_date,
      where: d.time <= ^end_date,
      order_by: [desc: d.id],

      select: %{sensor: d.sensor,time: d.time, angular_change: d.angular_change},
      limit: 3600
    )
  end



  def parse_data(topic, message) do

    Logger.info "SMS: Executing parse_data ..."
    Logger.info "SMS: Topic: #{inspect(topic)}"
    #sensor_data = Poison.decode!(message)

    # Logger.info "SMS: JSON Decode: #{inspect(sensor_data)}"

    case Poison.decode(message, keys: :atoms) do
      {:ok, sensor_data} ->
        Logger.info "SMS: The status was :ok! Decoding ..."

        Logger.info "SMS: JSON Decode: #{inspect(sensor_data)}"

        Logger.info "SMS: Iterate through Array..."
        Enum.each sensor_data, fn timepoint ->
          Logger.info "SMS: Iteration: #{inspect(timepoint)}"

          # write topic (therefore the name of the sensor) to the map
          timepoint = Map.put(timepoint, :sensor, List.last(topic))

          # unix time to UTC
          Logger.info "SMS: Updating UNIX time to UTC ... #{DateTime.from_unix!(timepoint.time)}"
          timepoint = Map.put(timepoint, :time, DateTime.from_unix!(timepoint.time))

          Logger.info "SMS: Updated time: #{inspect(timepoint)}"
          Logger.info "SMS: Broadcasting to room:lobby ..."

          if Map.has_key?(timepoint, :bpm) do
            if timepoint.bpm < 120 do
              Logger.info "SMS: Broadcasting BPM to room:lobby ..."
            else
              Logger.info "SMS: BPM higher than 100 - NOT broadcasting ..."
              Map.delete(timepoint, :bpm)
            end
          end
          SleepwebWeb.Endpoint.broadcast! "room:lobby", "new_msg", timepoint

          Logger.info "SMS: Writing to DB ..."
          create_data timepoint
        end

        {:ok, "Seems ok."}

      {:error, value} ->
        Logger.info "SMS: Decoding error."
        {:error, value}

      _ ->
        Logger.info "SMS: You passed in something else."
        {:unknown, "Unknown Error in JSON decoding."}
      end

  end


  # def parse_data(nicht json krams) do
  #
  #
  # end
end
