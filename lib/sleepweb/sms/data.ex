defmodule Sleepweb.SMS.Data do
  use Ecto.Schema
  import Ecto.Changeset


  schema "data" do
    field :angular_change, :float
    field :bpm, :float
    field :event, :string
    field :ibi, :float
    field :lin_acc, :float
    field :micro_vol, :float
    field :misc, :string
    field :sensor, :string
    field :time, :utc_datetime

    timestamps()
  end

  @doc false
  def changeset(data, attrs) do
    data
    |> cast(attrs, [:sensor, :time, :bpm, :ibi, :lin_acc, :micro_vol, :angular_change, :event, :misc])
    |> validate_required([:time])
  end
end
